const path = require('path');
const fs = require('fs');

const HTMLWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const ROOT = fs.realpathSync(process.cwd());
const isDev = process.env.NODE_ENV !== 'production';
const isAnalyze = process.argv.includes('--analyze');

function getFileName(ext) {
  return isDev ? `[name].[hash:8].${ext}` : `[name].[contenthash].${ext}`;
}

module.exports = {
  context: path.resolve(__dirname, 'src'),
  mode: isDev ? 'development' : 'production',
  resolve: {
    extensions: ['.js', '.json', '.jsx'],
  },
  entry: {
    index: path.resolve(ROOT, 'src/index.jsx'),
  },
  output: {
    filename: getFileName('js'),
    path: path.resolve(ROOT, 'dist'),
  },
  devServer: {
    open: true,
    port: 3000,
  },
  plugins: [
    new HTMLWebpackPlugin({
      // filename: 'index.html',
      template: path.resolve(ROOT, 'public/index.html'),
      // chunks: ['index'],
    }),
    new MiniCssExtractPlugin(),
    new CleanWebpackPlugin(),
    isAnalyze && new BundleAnalyzerPlugin(),
  ].filter(Boolean),
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: ['/node_modules/'],
        use: ['babel-loader'],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        loader: 'file-loader',
      },
      {
        test: /\.(ttf|woff2?|eot)$/i,
        loader: 'file-loader',
      },
    ],
  },
};
