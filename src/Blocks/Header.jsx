import React from 'react';

import headerLogo from '../assets/img/header.jpg';

export const Header = () => (
  <section className="header container">
    <div className="header__content">
      <h1 className="header__title">Product name</h1>
      <ul className="header__list">
        <li>Put on this page information about your product</li>
        <li>A detailed description of your product</li>
        <li>Tell us about the advantages and merits</li>
        <li>Associate the page with the payment system</li>
      </ul>
    </div>
    <div className="header__img">
      <img src={headerLogo} alt="header_image" />
    </div>
  </section>
);
