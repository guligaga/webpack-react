import React from 'react';
import reviewsLogo from '../assets/img/image.jpg';

const ReviewsArr = [];
for (let i = 0; i < 4; i++) {
  ReviewsArr.push(
    `Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum odio, iste quia 
    doloribus autem aperiam nulla ea neque reprehenderit. Libero doloribus, possimus officiis 
    sapiente necessitatibus commodi consectetur?`,
  );
}

const Component = shot => (
  <div className="reviews-item">
    <div className="reviews-item__img">
      <img src={reviewsLogo} alt="reviews_image" />
    </div>
    <div className="reviews-item__content">
      <p className="reviews-item__text">{shot.text}</p>
      <h4 className="reviews-item__author">Lourens S.</h4>
    </div>
  </div>
);

export const Reviews = () => (
  <section className="reviews container">
    <h3 className="reviews__title">Reviews</h3>
    {ReviewsArr.map(shot => (
      <Component text={shot} />
    ))}
  </section>
);
