import React from 'react';
import aboutLogo from '../assets/img/about.jpg';

export const About = () => (
  <section className="about container">
    <div className="about__content">
      <h3 className="about__title">About your product</h3>
      <p className="about__description">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis facilis fuga, illo at. Natus
        eos, eligendi illum rerum omnis porro ex, magni, explicabo veniam incidunt in quam sapiente
        ut ipsum.
      </p>
      <p className="about__description">
        Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem
        ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde
        perferendis nemo debitis dolor.
      </p>
    </div>
    <div className="about__img">
      <img src={aboutLogo} alt="about_image" />
    </div>
  </section>
);
