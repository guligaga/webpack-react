import React from 'react';

const buyList = [
  'Porro officia cumque sint deleniti',
  'Тemo facere rem vitae odit',
  'Cum odio, iste quia doloribus autem',
  'Aperiam nulla ea neque',
];

const listItems = buyList.map(item => <li>{item}</li>);

const Component = content => (
  <div className="buy-it-now-item">
    <h4 className="buy-it-now-item__title">{content.title}</h4>
    <p className="buy-it-now-item__price">{content.price}</p>
    <ol className="buy-it-now-item__list">{content.children}</ol>
  </div>
);

export const BuyItNow = () => (
  <section className="buy-it-now container">
    <div className="buy-it-now__content">
      <h3 className="buy-it-now__title">Buy it now</h3>
      <Component title="Standart" price="$100">
        {listItems}
      </Component>
      <Component title="Premium" price="$150">
        {listItems}
        {listItems}
      </Component>
      <Component title="Lux" price="$200">
        {listItems}
        {listItems}
        {listItems}
      </Component>
    </div>
  </section>
);
