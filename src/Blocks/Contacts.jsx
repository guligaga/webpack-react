import React from 'react';

import skypeLogo from '../assets/icons/skype_icon.png';
import icqLogo from '../assets/icons/icq_icon.png';
import emailLogo from '../assets/icons/email_icon.png';
import phoneLogo from '../assets/icons/phone_icon.png';
import socialLogo from '../assets/icons/social_buttons.png';

export const Contacts = () => (
  <section className="contacts container">
    <h3 className="contacts__title">Product name</h3>
    <form className="contacts__form">
      <input type="text" placeholder="Your name:" />
      <input type="text" placeholder="Your email:" />
      <textarea placeholder="Your message:" />
      <input type="button" value="SEND" />
    </form>
    <div className="contacts__links">
      <p>
        <img src={skypeLogo} alt="skype" />
        <span>here_your_login_skype</span>
      </p>
      <p>
        <img src={icqLogo} alt="icq" />
        <span>279679659</span>
      </p>
      <p>
        <img src={emailLogo} alt="email" />
        <span>psdhtmlcss@mail.ru</span>
      </p>
      <p>
        <img src={phoneLogo} alt="phone" />
        <span>80 00 4568 55 55</span>
      </p>
      <img src={socialLogo} alt="social" />
    </div>
  </section>
);
