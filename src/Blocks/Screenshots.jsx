import React from 'react';
import screenshotsLogo from '../assets/img/image.jpg';

const screenshotsArr = [];
for (let i = 0; i < 2; i++) {
  screenshotsArr.push(
    `Pariatur iure ab sunt nesciunt, quibusdam odio iste cumque itaque, ipsa vel exercitationem 
    ullam quos aut nostrum cupiditate fuga quaerat quam animi dolores. Sequi itaque, unde 
    perferendis nemo debitis dolor.`,
  );
  screenshotsArr.push(
    `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis facilis fuga, illo at. Natus 
    eos, eligendi illum rerum omnis porro ex, magni, explicabo veniam incidunt in quam sapiente ut 
    ipsum.`,
  );
}

const Component = shot => (
  <div className="screenshots-item">
    <div className="screenshots-item__img">
      <img src={screenshotsLogo} alt="screenshots_image" />
    </div>
    <div className="screenshots-item__content">
      <h4 className="screenshots-item__title">The description for the image</h4>
      <p className="screenshots-item__text">{shot.text}</p>
    </div>
  </div>
);

export const Screenshots = () => (
  <section className="screenshots container">
    <h3 className="screenshots__title">Screenshots</h3>
    {screenshotsArr.map(shot => (
      <Component text={shot} />
    ))}
  </section>
);
