import React from 'react';

const plusesArr = [];
for (let i = 0; i < 6; i++) {
  plusesArr.push(
    `Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque, impedit iure placeat, 
    ullam. Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime, quaerat porro 
    totam, dolore minus inventore.`,
  );
}

export const Pluses = () => (
  <div>
    <section className="pluses container">
      <h3 className="pluses__title">Dignity and pluses product</h3>
      <ul className="pluses__list">
        {plusesArr.map(plusesItem => (
          <li>{plusesItem}</li>
        ))}
      </ul>
    </section>
  </div>
);
