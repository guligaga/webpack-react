import React from 'react';

import './styles/App.scss';
import { Header } from './Blocks/Header';
import { About } from './Blocks/About';
import { Pluses } from './Blocks/Pluses';
import { Screenshots } from './Blocks/Screenshots';
import { Reviews } from './Blocks/Reviews';
import { BuyItNow } from './Blocks/BuyItNow';
import { Contacts } from './Blocks/Contacts';

export class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <About />
        <Pluses />
        <Screenshots />
        <Reviews />
        <BuyItNow />
        <Contacts />
      </div>
    );
  }
}
