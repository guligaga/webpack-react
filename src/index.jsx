import React from 'react';
import ReactDOM from 'react-dom';

import './styles/main.scss';
import { App } from './App';

ReactDOM.render(
  // eslint-disable-next-line react/jsx-filename-extension
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.querySelector('.wrapper'),
);
